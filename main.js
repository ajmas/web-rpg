const blockSize = 64;
let x = 1;
let y = 0;
let character;
let canvas;
let canvasCtx;
let sourceWidth= 16;
let sourceHeight= 18;
let characterImage;
let backgroundImage;
let scale = 2;
let scaledWidth = blockSize;
let scaledHeight = blockSize;

let mapBasePath = './maps/';
let frame = 0;
let orientation = 0;

// const map.dimensions = [10, 10];
let characterSize = { width: blockSize, height: blockSize };

const objectImages = {};
let map;

let teleports = {};
let compactedCM;

function replaceAtIndex(str, offset, str2) {
    const part1 = str.substring(0, offset);
    const part2 = str.substring(offset + str2.length);
    return part1 + str2 + part2;
}

// Take the objects an apply their collision maps to the base collision map
function recomputeCollisionMap () {
    map.objects.forEach(mapObject => {
        if (objectImages[mapObject.id] && objectImages[mapObject.id].complete) {
            if (mapObject.collisionMap) {
                let width =  mapObject.dimensions[0];
                const location = mapObject.location;

                for (let y = 0; y < mapObject.dimensions[1]; y++) {
                    const line = mapObject.collisionMap.substring(y * width, (y * width) + width);
                    const offset = ((location.y + y )* map.dimensions[0]) + location.x;
                    compactedCM = replaceAtIndex(compactedCM, offset, line);

                    // Set up the teleports
                    for (let x = 0; x < width; x++) {
                        const offset = ((location.y + y )* map.dimensions[0]) + location.x + x;
                        if (line[x] === 'A') {
                            teleports[`T${offset}`] = mapObject;
                        }
                    }
                }
            }
        }
    });
}

function canMove (newX, newY) {
    if (newX < -4 || newY < -4 || newX > (canvas.width - characterSize.width) || newY > (canvas.height - characterSize.height) ) {
        return false;
    }

    const charBounds = { x1: newX +3 , y1: newY + 3, x2: newX + blockSize, y2: newY + blockSize};
    for (let y = 0; y < map.dimensions[1]; y++) {
        for (let x = 0; x < map.dimensions[0]; x++) {
            const blockBounds = { x1: x * blockSize, y1: y * blockSize, x2: (x * blockSize) + blockSize, y2: (y * blockSize) + blockSize };

            const xIntersect = ((blockBounds.x1 < charBounds.x1 && blockBounds.x2 > charBounds.x1) || blockBounds.x1 < charBounds.x2 && blockBounds.x2 > charBounds.x2);
            const yIntersect = ((blockBounds.y1 < charBounds.y1 && blockBounds.y2 > charBounds.y1) || blockBounds.y1 < charBounds.y2 && blockBounds.y2 > charBounds.y2);

            if (compactedCM.charAt((y * map.dimensions[0]) + x) === '0' && xIntersect && yIntersect) {
                return false;
            }
        }
    }

    return true;
}

function drawActiveBlocks () {
    const charBounds = { x1: x + 3, y1: y + 3, x2: x + blockSize - 3, y2: y + blockSize - 3};

    for (let y = 0; y < map.dimensions[1]; y++) {
        for (let x = 0; x < map.dimensions[0]; x++) {
            const blockBounds = { x1: x * blockSize, y1: y * blockSize, x2: (x * blockSize) + blockSize, y2: (y * blockSize) + blockSize };

            const xIntersect = ((blockBounds.x1 < charBounds.x1 && blockBounds.x2 > charBounds.x1) || blockBounds.x1 < charBounds.x2 && blockBounds.x2 > charBounds.x2);
            const yIntersect = ((blockBounds.y1 < charBounds.y1 && blockBounds.y2 > charBounds.y1) || blockBounds.y1 < charBounds.y2 && blockBounds.y2 > charBounds.y2);

            if (xIntersect && yIntersect) {
                canvasCtx.beginPath();
                canvasCtx.lineWidth = 1;
                canvasCtx.fillStyle = '#00FF00AA';
                canvasCtx.fillRect(x * blockSize, y * blockSize, blockSize, blockSize);
                canvasCtx.stroke();
            }


            if (compactedCM.charAt((y * map.dimensions[0]) + x) === '0' && xIntersect && yIntersect) {
                canvasCtx.beginPath();
                canvasCtx.lineWidth = 1;
                canvasCtx.fillStyle = '#FF0000AA';
                canvasCtx.fillRect(x * blockSize, y * blockSize, blockSize, blockSize);
                canvasCtx.stroke();
            }
        }
    }
}

function drawCollisionMap() {
    const colours = {
        '1': '#fff0',
        '0': '#fff9',
        'A': '#f009',
        'B': '#0f09',
        'C': '#00f9',
        'D': '#0009'
    }
    for (let y = 0; y < map.dimensions[1]; y++) {
        for (let x = 0; x < map.dimensions[0]; x++) {
            canvasCtx.beginPath();
            canvasCtx.lineWidth = 1;
            const val = compactedCM.charAt((y * map.dimensions[0]) + x);
            if (val === '1') {
                canvasCtx.fillStyle = '#fff0';
                canvasCtx.strokeStyle = 'red';
                canvasCtx.lineWidth = 1;
                canvasCtx.fillRect(x * blockSize, y * blockSize, blockSize, blockSize);
            } else if (val === '0') {
                canvasCtx.strokeStyle = 'black';
                canvasCtx.fillStyle = '#fff9';
                canvasCtx.fillRect(x * blockSize, y * blockSize, blockSize, blockSize);
            } else {
                // canvasCtx.strokeStyle = 'black';
                canvasCtx.fillStyle = colours[val];
                canvasCtx.fillRect(x * blockSize, y * blockSize, blockSize, blockSize);
            }
            canvasCtx.stroke();
        }
    }
}

function pointToBlockCoord (x, y) {
    return [
        Math.floor(x / blockSize),
        Math.floor(y / blockSize)
    ]
}

// if this is a teleport block, then return destination
async function handleTeleport (xBlock, yBlock, orientation) {
    const offset = (yBlock * map.dimensions[0]) + xBlock;
    if (teleports[`T${offset}`] && orientation === 1) {
        const mapObject = teleports[`T${offset}`];
        if (mapObject.teleportTo.startsWith('http')) {
            window.location.href = mapObject.teleportTo;
        } else if (mapObject.teleportTo.startsWith('map:')) {
            await loadMap(mapObject.teleportTo.substring(4));
        }
    }
}

async function loadImage(url) {
    return new Promise((resolve, reject) => {
        // Create an image object. This is not attached to the DOM and is not part of the page.
        const image = new Image();

        // When the image has loaded, draw it to the canvas
        image.onload = function() {
            resolve(image);
        }

        image.onerror= function(error) {
            reject();
            console.log('BOOM', error);
        }

        // Now set the source of the image that we want to load
        image.src = url;

        return image;
    });
}

function drawBackground() {
    if (backgroundImage && backgroundImage.complete) {
        canvasCtx.drawImage(backgroundImage, 0, 0);
    }
}

function drawObjects() {
    map.objects.forEach(mapObject => {
        if (objectImages[mapObject.id] && objectImages[mapObject.id].complete) {

            canvasCtx.drawImage(objectImages[mapObject.id],
                0, 0, objectImages[mapObject.id].width, objectImages[mapObject.id].height,
                mapObject.location.x * blockSize, mapObject.location.y * blockSize,
                mapObject.dimensions[0] * blockSize, mapObject.dimensions[1] * blockSize);
        }
    });
}

function drawCharacterFrame(frameX, frameY, canvasX, canvasY) {
    canvasCtx.drawImage(characterImage,
        frameX * sourceWidth, frameY * sourceHeight, sourceWidth, sourceHeight,
        canvasX, canvasY, scaledWidth, scaledHeight );
}

function drawScene () {
    drawBackground();
    drawCollisionMap();
    drawActiveBlocks();
    drawObjects();
    drawCharacterFrame(frame, orientation, x, y);
}

function handleKeyPress (event) {
    const delta = 4;
    let oldOrientation = orientation;

    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);

    let newX = x;
    let newY = y;

    if (event.key === 'w' || event.key === 'W') {
        newY -= delta;
        orientation = 1;
    } else if (event.key === 'a' || event.key === 'A') {
        newX -= delta;
        orientation = 2;
    } else if (event.key === 'd' || event.key === 'D') {
        newX += delta;
        orientation = 3;
    } else if (event.key === 's' || event.key === 'WS') {
        newY += delta;
        orientation = 0;
    }

    if (oldOrientation === orientation) {
        frame++;
    } else {
        frame = 0;
    }

    if (canMove(newX, newY)) {
        x = newX;
        y = newY;
    }

    if (frame === 3) {
        frame = 0;
    }

    const blockCoord = pointToBlockCoord(x, (y + (blockSize / 2)));
    handleTeleport(blockCoord[0], blockCoord[1], orientation);
    drawScene();
}

async function initMap () {
    if (map.objects) {
        for await (const mapObject of map.objects) {
            objectImages[mapObject.id] = await loadImage(mapObject.image);
        };
    }

    compactedCM = map.collisionMap.replace(/ /g, '');
    recomputeCollisionMap();

    backgroundImage = await loadImage(map.background);
    characterImage = await loadImage('./resources/Green-Cap-Character-16x18.png');

    x = map.spawn[0] * blockSize;
    y = map.spawn[1] * blockSize;
}

async function loadMap (mapname) {
    const response = await axios.get(`${mapBasePath}/${mapname}.json`);

    if (!response || !response.data || !response.data.data) {
        throw new Error('Could not load map data');
    }

    map = response.data.data;

    await initMap();
}

async function init () {
    try {
        canvas = document.querySelector('.world-canvas');
        canvasCtx = canvas.getContext('2d');

        canvas.width = canvas.clientWidth * 2;
        canvas.height = canvas.clientHeight * 2;

        character = document.querySelector('.character');

        document.addEventListener('keypress', handleKeyPress);

        await loadMap('mapzero');

        drawScene();
    } catch (error) {
        console.error(error);
    }
}

document.addEventListener('DOMContentLoaded', init);

// ref: https://opengameart.org/sites/default/files/Green-Cap-Character-16x18.png
// ref: https://dev.to/martyhimmel/moving-a-sprite-sheet-character-with-javascript-3adg